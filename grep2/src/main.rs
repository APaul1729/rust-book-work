
// Simple implementation of grep
// testing out how to create a command-line program

use std::env;
use std::process;

use grep2::Config;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = grep2::run(config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}

