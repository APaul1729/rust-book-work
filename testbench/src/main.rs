/*
 * Testbench for rust
 *
 * i just want t olearn rust/string operations in it, so i'm
 * going to write a bunch of functions to do arg parsing and
 * arbitrary string manipulation
*/

// Resources:
// https://hermanradtke.com/2015/05/03/string-vs-str-in-rust-functions.html

#![allow(dead_code)]
#![allow(unused_must_use)]
#![allow(unused_imports)]

use std::io::{self, Read, Write};

// copy-on-write
use std::borrow::Cow;

use std::collections::HashSet;

// simple function to understand string/String ownership
fn pprint(msg: &str) { println!("msg = {}", msg); }

fn copy_input(input: &str) -> String {
    let mut buf = String::with_capacity(input.len());
    for c in input.chars() {
        buf.push(c);
    }

    buf
}

// remove spaces from input &str (borrowed)
fn remove_spaces<'a>(input: &'a str) -> Cow<'a, str> {
// to my understanding:
// with lifetime 'a, reference to 'input' lives that long
// copy-on-write will write something that has lifetime 'a
    if input.contains(' ') {
        let mut buf = String::with_capacity(input.len());

        for c in input.chars() {
            if c != ' ' {
                buf.push(c);
            }
        }

        return Cow::Owned(buf);
    }

    return Cow::Borrowed(input);
}

// see various forms of ownership
// using fn pprint(msg: str)
fn string_ownership() {
    let string = "hello world";
    pprint(string);

    let owned = "hello".to_string();
    //let owned = String::from_str("hello");
    pprint(&owned);

    let count = std::rc::Rc::new("hello".to_string());
    pprint(&count);

    let atm = std::sync::Arc::new("hello".to_string());
    pprint(&atm);
}

fn uniq_1(input: &str) -> String {
    // naive, unoptimized version of 'uniq' tool

    // worst-case size
    let mut holding = String::with_capacity(input.len());
    let mut compare = String::with_capacity(input.len());

    // full output buffer
    let mut buf = String::with_capacity(input.len());

    for c in input.chars() {
    // have to deal with problems like char accessing
    //for c in 0..input.len() {

        if c != '\n' {
            holding.push(c);
        } else {
            if ! (compare.trim() == holding.trim()) {
                compare.clear();
                compare.push_str(&holding);

                buf.push_str(&holding);
                buf.push('\n');
            }                /*
            if buf.as_bytes()[c_eol..] != holding.trim() {
                c_eol += holding.len();
                buf.push_str(&holding);
                buf.push('\n');
            }
            */
            holding.clear();
        }

        /*
        if input.as_bytes()[c] != '\n' {
            buf.push(c);
        }
        else if c_eol == 0 && input[c] == '\n' {
            buf.push(c);
        }
        else if input[c] == '\n' && c_eol != 0 {
        }
        */
    }

    buf
}

fn uniq_2 (input: &str) -> String {
    // different uniq approach - use set every line to check if anything's here
    let mut strings = HashSet::new();

    let mut size_s = 0;
    let mut holding = String::new();
    for c in input.chars() {
        if c != '\n' {
            holding.push(c);
        } else {
            //for s in &strings { println!("{}",s); }
            if ! strings.contains(&holding) {
                strings.insert(holding.to_string());
                size_s += holding.len() + 1;
            }
            holding.clear();
        }
    }
    
    let mut buf = String::with_capacity(size_s-1);
    for s in &strings {
        buf.push_str(&s);
        buf.push('\n');
    }
    buf
}

// todo
/// implement simple cli cat
fn cat_1 () {
}

fn call() -> io::Result <()> {

    let stdin = io::stdin();
    let stdout = io::stdout();

    let mut inbuf = String::new();

    let mut hin = stdin.lock();
    let mut hout = stdout.lock();

    // read input
    hin.read_to_string(&mut inbuf);

    // do something
    // todo - i should change outbuf into some Writer?
    let outbuf = uniq_2(&inbuf);

    // todo - figure out string slicing
    //let s1 = "hello world";
    //let s2 = "this is a longer string";
    //println!("c1: {}", s1[1]);

    // write output
    hout.write(outbuf.as_bytes());

    // put ok return here for now
    // todo - actually learn error handling
    Ok(())
}

fn main() {
    call();
}
