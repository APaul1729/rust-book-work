
use std::thread;
use std::sync::Arc;
use std::sync::Mutex;
use std::vec::Vec;
use std::sync::mpsc;

// give threads a job or a signal to term
enum Message {
	NewJob(Job),
	Terminate,
}

// job/FnBox uses a workaround because the rust compiler doesn't currently understand we're
// allowing ourselves to take ownership of the thing inside a Box<T>
// https://doc.rust-lang.org/book/ch20-02-multithreaded.html

type Job = Box<FnBox + Send + 'static>;

// accepts self: Box<Self> to take ownership of self move value out of Box<T>
// (i dont really understand this)
trait FnBox {
    fn call_box(self: Box<Self>);
}

// implement FnBox trait for any type F that implements the FnOnce trait
impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

struct Worker {
    id: usize,
    //jh: thread::JoinHandle<()>,
	jh: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || {
            loop {
                let message = receiver.lock().unwrap().recv().unwrap();

				match message {
					Message::NewJob(job) => {
						println!("Worker {} received job. Executing...", id);

						// this code errors (rust 1.34) because Rust compiler doesn't
						// understand we can take ownership of Box<Self> ?
						// (*job)();
						job.call_box();
					},
					Message::Terminate => {
						println!("Worker {} terminating...", id);
						break;
					},
				}
            }
        });

        Worker {
            id : id,
            jh : Some(thread),
        }
    }
}

// threadpool
pub struct ThreadPool {
    threads: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

impl Drop for ThreadPool {
	fn drop(&mut self) {
		println!("Sending shutdown to workers in pool.");

		for _ in &mut self.threads {
			self.sender.send(Message::Terminate).unwrap();
		}

		println!("Shutting down workers in pool.");

		for worker in &mut self.threads {
			println!("Shutting down worker: {}", worker.id);
			if let Some(thread) = worker.jh.take() {
				thread.join().unwrap();
			}
		}
	}
}

impl ThreadPool {
    /// Create new ThreadPool
    ///
    /// size is number of threads in pool
    ///
    /// # Panics
    ///
    /// The `new` function will panic if size == 0
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();

        let receiver = Arc::new(Mutex::new(receiver));

        let mut threads = Vec::with_capacity(size);

        for i in 0..size {
            // create threads, push to vector
            threads.push(Worker::new(i, Arc::clone(&receiver)));
        }

        ThreadPool {
            threads,
            sender,
        }
    }

    pub fn execute<F>(&self, f: F)
        where
            F: FnOnce() + Send + 'static
    {
        let job = Box::new(f);

        self.sender.send(Message::NewJob(job)).unwrap();
    }
}
