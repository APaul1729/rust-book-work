
// resource:
// https://doc.rust-lang.org/book/ch20-01-single-threaded.html

use std::fs;
use std::thread;
use std::time::Duration;
use std::io::prelude::*;
use std::net::TcpStream;
use std::net::TcpListener;

// own implementation
use webserver::ThreadPool;

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 512];
    stream.read(&mut buffer).unwrap();

    let get_root = b"GET / HTTP/1.1\r\n";
    let get_query = b"GET /query HTTP/1.1\r\n"; // simulate a slow query to try multithreading

    let ok = "HTTP/1.1 200 OK\r\n\r\n";
    let err = "HTTP/1.1 404 NOT FOUND\r\n\r\n";

    let (root, status, fname) = if buffer.starts_with(get_root) {
        ("/", ok, "src/hello.html")
    } else if buffer.starts_with(get_query) {
        thread::sleep(Duration::from_secs(5));
        ("/query", ok, "src/hello.html")
    } else {
        ("Unknown", err, "src/404.html")
    };

    println!("HTTP: {}", root);
    let contents = fs::read_to_string(fname).unwrap();
    let response = format!("{}{}", status, contents);
    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    let pool = ThreadPool::new(4);

    for stream in listener.incoming().take(2) {
        let stream = stream.unwrap();

        pool.execute(|| {
            //println!("Connection established.");
            handle_connection(stream)
        });
    }
}
